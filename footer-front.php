</div>
<footer id="footer2" aria-label="footer">
	<div class="footer_interior">

		<div class="home_connect">
            <div class="left">
                <h1>Stay Connected</h1>

                <div class="sc_left">
                	<h2>Find Us</h2>

                    <address>
                        <strong>Research Manitoba</strong><br>
						A201 Chown Building<br>
						753 McDermot Avenue<br>
						Winnipeg, MB R3E 0T6
						
                    </address>

                    <span class="contact_line"><strong>Telephone</strong> <a href="tel:2047751096">204 775 1096</a></span>
                    <span class="contact_line"><strong>Fax</strong> 204 786 5401</span>
                    <span class="contact_line"><strong>Email</strong> <a href="mailto:info@researchmb.ca">info@researchmb.ca</a></span>

                    <a target="_blank" href="https://www.google.ca/maps/place/Chown+Building/@49.9038361,-97.1632384,17z/data=!4m12!1m6!3m5!1s0x52ea73dcf079c7d3:0x50ce439c4036be90!2sChown+Building!8m2!3d49.9038327!4d-97.1610497!3m4!1s0x52ea73dcf079c7d3:0x50ce439c4036be90!8m2!3d49.9038327!4d-97.1610497?hl=en" class="map">Map</a><br>

                </div>

              <div class="sc_right">
               		<h2>Follow Us</h2>

              		<?php get_template_part( 'template-parts/components/social-media-footer' );?>
              </div>

            </div>

            <div class="right">
                <h1>Ask Anything</h1>
				
                <div id="ask_anything">	
                	<?php echo do_shortcode('[contact-form-7 id="3287" title="Ask Anything"]'); ?>
                </div>
            </div>
        </div>

        <div class="footer_list-container">
	    	<ul class="footer_list">
	        	<li>
	            	<h1>About</h1>

	                <ul class="sub_footer_list">
	                	<?php 
							$pages = get_posts('numberposts=500&orderby=menu_order&post_type=page&post_parent='.PAGE_ABOUT); 

							foreach ($pages as $key => $value) : 		
						?>
	                    <li><a href="<?php echo get_permalink($value->ID); ?>"><?php echo $value->post_title; ?></a></li>
	                    <?php endforeach; ?>
	                </ul>
	            </li>

	            <li>
	            	<h1>Funding</h1>

	                <ul class="sub_footer_list">
	                	<?php 
							$pages = get_posts('numberposts=500&orderby=menu_order&post_type=page&post_parent='.PAGE_FUNDING); 

							foreach ($pages as $key => $value) : 		
						?>
	                    <li><a href="<?php echo get_permalink($value->ID); ?>"><?php echo $value->post_title; ?></a></li>
	                    <?php endforeach; ?>
	                </ul>
	            </li>

	            <li>
	            	<h1>Initiatives</h1>

	                <ul class="sub_footer_list">
	                    <?php 
							$pages = get_posts('numberposts=500&orderby=menu_order&post_type=page&post_parent='.PAGE_INITIATIVES); 

							foreach ($pages as $key => $value) : 		
						?>
	                    <li><a href="<?php echo get_permalink($value->ID); ?>"><?php echo $value->post_title; ?></a></li>
	                    <?php endforeach; ?>
	                </ul>
	            </li>

	            <li>
	            	<h1>Impacts</h1>

	                <ul class="sub_footer_list">
	                	<?php 
							$pages = get_posts('numberposts=500&orderby=menu_order&post_type=page&post_parent='.PAGE_IMPACTS); 

							foreach ($pages as $key => $value) : 		
						?>
	                    <li><a href="<?php echo get_permalink($value->ID); ?>"><?php echo $value->post_title; ?></a></li>
	                    <?php endforeach; ?>
	                </ul>
	            </li>

	            <li>
	            	<h1>Partnerships</h1>

	                <ul class="sub_footer_list">
	                	<?php 
							$pages = get_posts('numberposts=500&orderby=menu_order&post_type=page&post_parent='.PAGE_PARTNERSHIPS); 

							foreach ($pages as $key => $value) : 		
						?>
	                    <li><a href="<?php echo get_permalink($value->ID); ?>"><?php echo $value->post_title; ?></a></li>
	                    <?php endforeach; ?>
	                </ul>
	            </li>

	            <li>
	            	<h1>Events Calendar</h1>

	                <ul class="sub_footer_list">
	                	<?php 
							$pages = get_posts('numberposts=500&orderby=menu_order&post_type=page&post_parent='.PAGE_EVENTS_CAL); 

							foreach ($pages as $key => $value) : 		
						?>
	                    <li><a href="<?php echo get_permalink($value->ID); ?>"><?php echo $value->post_title; ?></a></li>
	                    <?php endforeach; ?>
	                </ul>
	            </li>

	            <li class="print">
	            	<h1>Print Page</h1>

	                <a onclick="printpage();" class="print_page">Print Page</a>
	            </li>
	        </ul>
	    </div>
    </div>

    <div class="footer_lower">
    	<a href="http://www.honestagency.com/" class="left" target="_blank">Site by Honest Agency</a>

        <?php
			$copyright = '2012';
			if (date('Y') > 2012) {
				$copyright .= '-'.date('Y');
			}
		?>
        <span class="right">Research Manitoba. Copyright &copy; <?php echo $copyright; ?>. All rights reserved.</span>
    </div>
</footer>
			
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/jquery/1.7/jquery.min.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/template/fancybox/lib/jquery.mousewheel-3.0.6.pack.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/template/fancybox/source/jquery.fancybox.pack.js?v=2.0.6"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/template/fancybox/source/helpers/jquery.fancybox-buttons.js?v=1.0.2"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/template/fancybox/source/helpers/jquery.fancybox-media.js?v=1.0.0"></script>
<script type="text/javascript" src="//ajax.googleapis.com/ajax/libs/swfobject/2.2/swfobject.js"></script>
<script type="text/javascript" src="<?php bloginfo('template_url'); ?>/template/js/life.js"></script>
<?php wp_footer(); ?>



			</body>
			</html>