<?php
/**
 * Template Name: Login
 */

get_header(); 


if ( have_posts() ) {
	while ( have_posts() ) {
		the_post();
	}
}

$the_parent = $post->post_parent;
$id = get_the_ID();


if ($the_parent == 0) {
	$the_parent = $id;
}

$title = get_the_title();
$content = get_the_content();

$ancestors = get_post_ancestors( $id );
$ancestor_id = end($ancestors);

$template_colour = get_post_meta($ancestor_id, 'template_colour', true);
if (!$template_colour) {
	$template_colour = 'purple';
}


$banner = get_post_meta($id, 'banner', true);
if (!$banner) {
    $banner = get_post_meta($ancestor_id, 'banner', true);
}
if( substr( $banner, 0, 4 ) === "http" ) {
    $banner_out = $banner;
} else {
    $banner_out = get_bloginfo('url') . $banner;
}
$sidebar = get_post_meta($the_parent, 'sidebar', true);

?>


	<?php if(is_user_logged_in()) { ?>


		<div class="main_content">
        	
            
        	<div class="holder blue">
            	<div class="copy_content">
            		<h1>Member Section</h1>
                	
                	<?php echo wpautop(get_the_content()); ?>
                    
                </div>

                <aside>
                    <?php
                        $taxonomy = 'wpdmcategory';
                        $taxonomy_args = array(
                            'hide_empty' => 0,
                            'orderby' => 'name', 
                            'order' => 'ASC',
                        );
                        $tax_terms = get_terms($taxonomy, $taxonomy_args);
                    ?>
                    <ul class="file-categories">
                        <?php
                            $current_url = 'http'.(empty($_SERVER['HTTPS'])?'':'s').'://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
                            foreach ($tax_terms as $tax_term) {
                                $page_url = get_bloginfo('url') . '/download-category/' . $tax_term->slug . '/';

                                if( $page_url == $current_url ) {
                                    $class = 'class="active"';
                                } else {
                                    $class = '';
                                }
                                ?>
                                <li>
                                    <a <?php echo $class; ?> href="<?php echo $page_url; ?>"><?php echo $tax_term->name; ?></a>
                                </li>
                            <?php } ?>
                            <li>
                            	<a href="<?php echo wp_logout_url( get_bloginfo('url') . '/board-login/'); ?>">Logout</a>
                            </li>
                    </ul>

                </aside>
            </div>
        </div>
        
        <div class="clear_both"></div>



	<?php } else { ?>

		<div class="main_content">
        	<!-- <img src="<?php bloginfo('url'); ?><?php echo $banner; ?>" class="sub_banner"> -->
            
        	<div class="holder <?php echo $template_colour; ?>">
            	<div class="copy_content">
                	
                	<?php echo do_shortcode('[wp-members page="login"]'); ?>
                    
                </div>
            </div>
        </div>
        
        <div class="clear_both"></div>


   <?php } ?>
                
<?php get_footer(); ?>

<script type="text/javascript">
function check_my_pass() {
	var password = document.getElementById('password');
	var password1 = document.getElementById('password1');
	
	if (password.value == '' || password1.value == '') {
		password.style.color = '#f40505';
		password.style.border = '1px solid #f40505';
			
		password1.style.color = '#f40505';
		password1.style.border = '1px solid #f40505';
			
		return false;
	}
	else {
		if (password.value == password1.value) {
			return true;
		}
		else {
			password.style.color = '#f40505';
			password.style.border = '1px solid #f40505';
			
			password1.style.color = '#f40505';
			password1.style.border = '1px solid #f40505';
			
			return false;
		}
	}
}
</script>
