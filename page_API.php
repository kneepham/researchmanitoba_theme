<?php
/**
 * Template Name: API
 */

$id_num = $_REQUEST['id'];
if (!is_numeric($_REQUEST['id'])) {
	exit();
}

$page = get_page($id_num);

if (is_page(API_RECIP_AWARDED)) {
	$recipient_content = get_posts('numberposts=500&orderby=menu_order&post_type=page&post_parent='.$id_num);
	
	foreach ($recipient_content as $key => $value) : 
		$job_title = get_post_meta($value->ID, 'job_title', true);
		$project_title = get_post_meta($value->ID, 'project_title', true);
										
        echo '<li>';
        echo '<h2>'.$value->post_title.'</h2>';
        echo '<h3>'.$job_title.'</h3>';
        echo '<p><strong><em>'.$project_title.':</em></strong> '.ttruncat($value->post_content, 160).' <a href="'.home_url( '/' ).'?page_id='.$value->ID.'" class="more">More -></a></p>';
        echo '</li>';
	endforeach;
}
elseif (is_page(API_RECIP_CAT)) {
	echo '<option value="==">'.$page->post_title.' Awards Categories</option>'."\r\n";
	echo '<option value="==">==</option>'."\r\n";
	
	$recipient_cats = get_posts('numberposts=500&orderby=menu_order&post_type=page&post_parent='.$id_num);
	
	foreach ($recipient_cats as $key => $value) : 
	    echo '<option value="'.$value->ID.'">'.$value->post_title.'</option>'."\r\n";
    endforeach;
}
?>