<?php
/**
 * Template Name: Recipients
 */
get_header(); 

if ( have_posts() ) {
	while ( have_posts() ) {
		the_post();
	}
}

$the_parent = $post->post_parent;
$id = get_the_ID();

if ($the_parent == 0) {
	$the_parent = $id;
}

$title = get_the_title();
$content = get_the_content();

$ancestors = get_post_ancestors( $id );
$ancestor_id = end($ancestors);

$template_colour = get_post_meta($ancestor_id, 'template_colour', true);
if (!$template_colour) {
	$template_colour = 'purple';
}

$banner = get_post_meta($id, 'banner', true);
if (!$banner) {
	$banner = get_post_meta($ancestor_id, 'banner', true);
}
if( substr( $banner, 0, 4 ) === "http" ) {
    $banner_out = $banner;
} else {
    $banner_out = get_bloginfo('url') . $banner;
}

$sidebar = get_post_meta($the_parent, 'sidebar', true);

$recipient_pages = get_posts('numberposts=500&orderby=menu_order&post_type=page&post_parent='.$id);
$recipient_content = get_posts('numberposts=500&orderby=menu_order&post_type=page&post_parent='.$recipient_cats[0]->ID);

$recipients = $wpdb->get_results( "SELECT id, r_year FROM mhrc_recipients WHERE trashed='n'");

//$recipients_years = array();
$yearz_b = array();

foreach($recipients as $key => $value) {
	//$recipients_years[$value->r_year][] = $value->id;
	$yearz_b[] = $value->r_year;
}

//krsort($recipients_years);

$yearz = array_unique($yearz_b);
rsort($yearz);
unset($yearz_b);

$recipient_cats = $wpdb->get_results( "SELECT id, r_year, r_category FROM mhrc_recipients WHERE r_year=".$yearz[0]." && trashed='n' GROUP BY r_category ORDER BY r_category");

?>

				<div class="main_content">
                	
                    
                	<div class="holder <?php echo $template_colour; ?>">
                    	<div class="copy_content">
                        	<h1><?php echo $title; ?></h1>
                            <h2 id="r_category" style="display:none;"><?php //echo $recipient_cats[0]->post_title; ?></h2>
                            
                            <ul id="r_list" class="recipient">
                            	<li class="default"><?php echo $content; ?></li>
                                
                                <!--<?php 
									foreach ($recipient_content as $key => $value) : 
										$job_title = get_post_meta($value->ID, 'job_title', true);
										$project_title = get_post_meta($value->ID, 'project_title', true);
								?>
                                <li>
                                    <h2><?php echo $value->post_title; ?></h2>
                                    <h3><?php echo $job_title; ?></h3>
                                    
                                    <p><strong><em><?php echo $project_title; ?>:</em></strong> <?php echo ttruncat($value->post_content, 160); ?> <a href="<?php echo get_permalink($value->ID); ?>" class="more">More -></a></p>
                                </li>
                                <?php endforeach; ?>-->
                            </ul>
                        </div>
                        
                        <aside>
                        	<div class="select_box_grad">
                        		<span class="select_wrap">
                        			<select id="year_selector" name="year_selector" onclick="recipients_load_year(this);">
                                		<?php foreach ($yearz as $key => $value) : ?>
                                		<option value="<?php echo $value; ?>">Award Year - <?php echo $value; ?></option>                               		
		                                <?php endforeach; ?>
                                	</select>
                        		</span>
                        	</div>
                        	<div class="select_box_grad">
                                <span class="select_wrap">
                                    <select id="year_box" name="year_box" data-homeurl="<?php home_url( '/' ); ?>" onchange="recipients_load_category(this);" onselect="recipients_load_category(this);">
                                        <option value="=="><?php echo $yearz[0]; ?> Awards Categories</option>
                                        <option value="==">==</option>
                                        <?php foreach ($recipient_cats as $key => $value) : ?>
                                        <option value="<?php echo $value->r_category.'|'.$value->r_year.'|'.$value->id; ?>"><?php echo $value->r_category; ?></option>
                                        <?php endforeach; ?>
                                    </select>
                                </span>
                            </div>
                            
                            <?php 
								//foreach ($sidebar as $key => $value) : 
								foreach ($sidebar_am as $key => $value) : 
									//$sidebar_data = explode('|', $value);
									$sidebar_data = explode('|', $value->meta_value);
									$scount = count($sidebar_data);
									
									if ($scount == 1) :
										echo '<div class="sidebar-item">' . $sidebar_data[0] . '</div>';
									elseif ($scount == 2) : 
							?>
                            
                            	<a href="<?php echo $sidebar_data[1]; ?>" class="sidebar_img_px1" target="_blank">
                                    <img src="<?php echo $sidebar_data[0]; ?>">                                    
                                </a>
                                
                            <?php elseif ($scount == 3 || $scount == 4) : ?>
                                
                                <a href="<?php echo $sidebar_data[2]; ?>" class="sidebar_img_px2" target="_blank">
                                    <img src="<?php echo $sidebar_data[1]; ?>">                                    
                                    
                                    <h1><?php echo $sidebar_data[0]; ?></h1>
                                    
                                    <?php if ($sidebar_data[3]) : ?>
                                    <h2><?php echo $sidebar_data[3]; ?></h2>
                                    <?php endif; ?>
                                </a>
                                
                           <?php      
						   		   endif;	
								endforeach; 
								
								if ($sidebar_inc) {
									include $sidebar_inc;
								}
							?>
                        </aside>
                    </div>
                </div>
                
                <div class="clear_both"></div>
                
<?php get_footer(); ?>

<script type="text/javascript">
function recipients_load_year(element_ref) {
	var id_num = element_ref.value;
	var year_box = document.getElementById('year_box');
	year_box.innerHTML = '<option value="==">Loading...</option>';
	
	var strURL = '<?php echo bloginfo( 'template_url' ); ?>/process/r_load_year.php?id='+id_num;
	var req = getXMLHTTP();
		
	if (req) {
		req.onreadystatechange = function() {
			if (req.readyState == 4) {
				// only if "OK"
				if (req.status == 200) {
					$('#year_box').html(req.responseText);
				}		
			}				
		}			
	
		req.open("GET", strURL, true);
		req.send(null);
	}
}

function recipients_load_category(element_ref) {
	if (element_ref.value != '==' && element_ref.value != '') {
		var url_encode = element_ref.getAttribute('data-homeurl');
		var strURL = '<?php echo bloginfo( 'template_url' ); ?>/process/r_load_category.php?id='+element_ref.value+'&url='+ encodeURIComponent(url_encode);
		
		var req = getXMLHTTP();
			
		if (req) {
			req.onreadystatechange = function() {
				if (req.readyState == 4) {
					// only if "OK"
					if (req.status == 200) {
						var theContents = element_ref[element_ref.selectedIndex].innerHTML;
						
						var r_category = document.getElementById('r_category');
						r_category.innerHTML = theContents;
						r_category.style.display = 'block';
									
						var r_list = document.getElementById('r_list');
						r_list.innerHTML = req.responseText;
					}		
				}				
			}			
		
			req.open("GET", strURL, true);
			req.send(null);
		}
	}
}
</script>