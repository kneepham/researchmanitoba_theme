<?php get_header(); ?>

<div class="main_content">
<img src="<?php bloginfo('template_url'); ?>/template/img/blog.jpg" class="sub_banner">

	<div class="holder <?php echo $template_colour; ?>">
		<div class="copy_content">

				<h2 class="blogTop"><?php
					printf( __( 'Category Archives: %s', 'twentyten' ), single_cat_title( '', false ) );
				?></h2>

				<?php
					$category_description = category_description();
					if ( ! empty( $category_description ) )
						echo '' . $category_description . '';

				get_template_part( 'loop', 'category' );
				?>
		</div>

		<aside>
		<?php get_sidebar(); ?>
		</aside>
	</div>
</div>

<div class="clear_both"></div>
                
<?php get_footer(); ?>