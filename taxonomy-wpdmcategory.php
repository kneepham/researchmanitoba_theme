<?php
/**
 * Template Name: Council Files
 */
get_header();

global $post;
$slug = get_post( $post )->post_name;

$the_parent = $post->post_parent;
$id = get_the_ID();

if ($the_parent == 0) {
	$the_parent = $id;
}

$term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
$term_id = $term->term_id;
$term_tax = $term->taxonomy;
$title = $term->name;
$content = get_the_content();

$ancestors = get_post_ancestors( $id );
$ancestor_id = end($ancestors);

$file_query_args = array(
    'post_type' => 'wpdmpro',
    'status' => 'publish',
    'tax_query' => array(
        array(
            'taxonomy' => 'wpdmcategory',
            'field'    => 'id',
            'terms'    => get_queried_object()->term_id,
        ),
    ),
);

$file_query = new WP_Query( $file_query_args );


?>

<div class="main_content">
	<!-- <img src="<?php bloginfo('url'); ?>/site-files/2012/07/council.jpg" class="sub_banner"> -->
    
	<div class="holder blue">
    	<div class="copy_content">
            <h1>Member Section</h1>

            <?php if( !is_user_logged_in() ) { ?>

                <p>Please <a href="<?php bloginfo('url'); ?>/board-login">login</a> to view this page.</p>

            <?php } else { ?>
            	<h2 class="file-category-title"><?php echo $title; ?></h2>

                <?php
                $term_description = term_description( $term_id, $term_tax );
                if($term_description) { ?>
                    <div class="file-category-description">
                        <?php echo $term_description; ?>
                    </div>
                <?php } ?>

                <?php if ( $file_query->have_posts() ) : while ( $file_query->have_posts() ) : $file_query->the_post(); ?>
                    <?php
                    $num_comments = get_comments_number();
                    if ( $num_comments == 0 ) {
                        $comments = __('Leave a comment');
                    } elseif ( $num_comments > 1 ) {
                        $comments = $num_comments . __(' comments');
                    } else {
                        $comments = __('1 comment');
                    }
                    ?>

                    <div class="file-wrap <?php if( $file_query->current_post == '0') { ?>file-wrap-first<?php } ?>">
                        <?php echo do_shortcode('[wpdm_package id="' . get_the_ID() . '" template="5661ec76d81f7"]'); ?>
                        <a class="file-comments-link" href="#"><?php echo $comments; ?></a>
                    </div>

                    <div class="file-comments">
                        <?php
                        global $withcomments;
                        $withcomments = true;
                        comments_template( '', true );
                        ?>
                    </div>                                
                <?php endwhile; $file_query->wp_reset_postdata(); else: ?>
                    <div class="file-wrap">
                        <p>Nothing posted yet. Check back soon!</p>
                    </div>
                <?php endif; ?>
                
            </div>
            
            <aside>

                <?php
                    $taxonomy = 'wpdmcategory';
                    $taxonomy_args = array(
                        'hide_empty' => 0,
                        'orderby' => 'name', 
                        'order' => 'ASC',
                    );
                    $tax_terms = get_terms($taxonomy, $taxonomy_args);
                ?>
                <ul class="file-categories">
                    <?php
                        $current_url = 'http'.(empty($_SERVER['HTTPS'])?'':'s').'://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
                        foreach ($tax_terms as $tax_term) {
                            $page_url = get_bloginfo('url') . '/download-category/' . $tax_term->slug . '/';

                            if( $page_url == $current_url ) {
                                $class = 'class="active"';
                            } else {
                                $class = '';
                            }
                            ?>
                            <li>
                                <a <?php echo $class; ?> href="<?php echo $page_url; ?>"><?php echo $tax_term->name; ?></a>
                            </li>
                        <?php } ?>
                        <li>
                            <a href="<?php echo wp_logout_url( get_bloginfo('url') . '/board-login/'); ?>">Logout</a>
                        </li>
                </ul>

            </aside>

        <?php } ?>
    </div>
</div>

<div class="clear_both"></div>
                
<?php get_footer(); ?>
