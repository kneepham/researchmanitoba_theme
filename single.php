<?php get_header(); ?>

<div class="main_content">
<?php if ( has_post_thumbnail() ) { 
	the_post_thumbnail();    
} else { ?>
	<img src="<?php bloginfo('template_url'); ?>/template/img/blog.jpg" class="sub_banner">
<?php } ?>
	<div class="holder <?php echo $template_colour; ?>">
		<div class="copy_content">
			<style>
			img{
				max-width: 100%;
    			height: auto;
    		}
    		div.copy_content a.wpdm-download-link{
    			float:none;
    		}
			</style>
			<h2 class="blogTop"><?php echo get_the_title() ?></h2>
			<?php get_template_part( 'loop', 'single' ); ?>
				
			<?php the_content(); ?>
		</div>

		<aside>
		<?php get_sidebar(); ?>
		</aside>
	</div>
</div>

<div class="clear_both"></div>
                
<?php get_footer(); ?>