<?php
/**
 * Template Name: Password Reset
 */
get_header(); 

if ( have_posts() ) {
	while ( have_posts() ) {
		the_post();
	}
}

$the_parent = $post->post_parent;
$id = get_the_ID();


if ($the_parent == 0) {
	$the_parent = $id;
}

$title = get_the_title();
$content = get_the_content();

$ancestors = get_post_ancestors( $id );
$ancestor_id = end($ancestors);

$template_colour = get_post_meta($ancestor_id, 'template_colour', true);
if (!$template_colour) {
	$template_colour = 'purple';
}

$banner = get_post_meta($id, 'banner', true);
if (!$banner) {
	$banner = get_post_meta($ancestor_id, 'banner', true);
}
if( substr( $banner, 0, 4 ) === "http" ) {
    $banner_out = $banner;
} else {
    $banner_out = get_bloginfo('url') . $banner;
}

$sidebar = get_post_meta($the_parent, 'sidebar', true);

?>

				<div class="main_content">
                	
                    
                	<div class="holder <?php echo $template_colour; ?>">
                    	<div class="copy_content">
                    		<div id="lostPassword">
                    			<h2 class="form-title">Board Login</h2>
                    			<h4 class="form-sub-title">Forgot Your Password?</h4>
								
								<div id="message"></div>
								
								<form id="lostPasswordForm" method="post">
									<?php
										// this prevent automated script for unwanted spam
										if ( function_exists( 'wp_nonce_field' ) ) 
											wp_nonce_field( 'rs_user_lost_password_action', 'rs_user_lost_password_nonce' );
									?>

									<p>
										<label for="user_login">Enter your email address below and we’ll send you a link to reset your password.</label>
										<input type="text" name="user_login" id="user_login" class="form-input" value="<?php echo esc_attr($user_email); ?>" placeholder="Email Address" />
									</p>
									<?php
									/**
									 * Fires inside the lostpassword <form> tags, before the hidden fields.
									 *
									 * @since 2.1.0
									 */
									do_action( 'lostpassword_form' ); ?>
									<p class="submit">
										<input type="submit" name="wp-submit" id="wp-submit" class="form-submit" value="<?php esc_attr_e('Send Reset Link'); ?>" />
										<img src="<?php echo get_stylesheet_directory_uri(); ?>/template/img/loading.gif" id="preloader" alt="Preloader" />
									</p>
								</form>
							</div>

                        </div>


                    </div>
                </div>
                
                <div class="clear_both"></div>
                
<?php get_footer(); ?>
