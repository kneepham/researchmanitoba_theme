<?php

$category_id = get_field('category');
$post_limit = get_field('limit') ? get_field('limit') : -1;

$args = array(
	'post_type' 		=> 'magazine',
	'posts_per_page'	=>	$post_limit,
	'meta_key' 			=> 'upload_date',
	'orderby'			=> 'upload_date',
	'order'				=> 'DESC',
	'tax_query' => array(
		array(
			'taxonomy' => 'magazine_category',
			'field'    => 'id',
			'terms'    => $category_id,
		),
	)
);

$magazine_query = new WP_Query($args);
$read_more = get_field('read_more');

?>

<?php if ($magazine_query->have_posts()): ?>
<div class="magazine">

	<?php while($magazine_query->have_posts()): $magazine_query->the_post() ?>
		<?php get_excluded_magazine_post(get_the_ID()) ?>
		<?php $link = get_field(get_field('link_type')) ?>
		
		<?php if ($img = get_field('image')): ?>

			<div class="magazine__item">
				<a class="magazine__item-link" href="<?php echo $link ?>">
					<?php echo get_image_acf($img,'magazine-thumb',array('class' => "magazine__item-image")) ?>
					<div class="magazine__item-info-window">
						<h2 class="magazine__item-title"><?php echo get_the_title() ?>
				
							<?php if(get_field('use_category')): ?>
								<?php $terms = get_the_terms(get_the_ID(),'magazine_category') ?>
								<?php foreach ($terms as $term): ?>
									<br><?php echo $term->name ?>
								<?php break; endforeach ?>
							<?php endif ?>
						</h2>
						<h3 class="magazine__item-subtitle"><?php echo get_field('issue_title_date') ? get_field('issue_title_date')  : $read_more ?></h3>
					</div>

				</a>
			</div>

		<?php endif ?>

	<?php endwhile; wp_reset_postdata(); ?>

</div>
<?php endif ?>