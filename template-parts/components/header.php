<header class="header">
	<div class="container-main">

		<?php if ($logo = get_field('logo','options')): ?>
		<div class="header__logo"><a href="<?php echo get_home_url() ?>"><?php echo get_image_acf($logo) ?></a></div>
		<?php endif ?>

		<nav class="header__navigation" role="navigation">
			<?php
				$navigation = wp_nav_menu( array(
					'theme_location' => 'primary',
					'menu_id'        => 'primary',
					'container' => '',
					'echo' => false
				) );

				echo $navigation;
			?>
		</nav>

		<button class="hamburger hamburger--elastic header__hamburger" type="button"
		        aria-label="Menu" >
			<span class="hamburger-box">
				<span class="hamburger-inner"></span>
			</span>
			<span class="header__hamburger-title">Menu</span>
		</button>
		
	</div>
</header>

<?php get_template_part( 'template-parts/components/drawer') ?>