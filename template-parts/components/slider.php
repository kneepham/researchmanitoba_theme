<?php
	$slider_id = get_field('slider');
?>
<div class="slider slick__slider" 
	data-autoplay="<?php echo get_field('autoplay',$slider_id) ? "true" : "false" ?>" 
	data-pager="<?php echo get_field('pager',$slider_id) ? "true" : "false" ?>" 
	data-infinite="<?php echo get_field('infinite',$slider_id) ? "true" : "false" ?>"
	data-interval="<?php echo get_field('interval',$slider_id) ?>"
	data-transition-speed="<?php echo get_field('transition_speed',$slider_id) ?>"
	data-transition-type="<?php echo get_field('transition_type',$slider_id) == 'fade' ? "true" : "false" ?>"
>
	<?php if (have_rows('slides', get_field('slider'))): ?>

		<?php while(have_rows('slides', $slider_id)): the_row(); ?>
			<?php $img = get_sub_field('image'); ?>
			<div class="slide <?php echo get_sub_field('background_style') ?>">
				<a class="slide__a"href="<?php echo get_sub_field('link') ?>">
					<?php if ($img): ?><?php echo get_image_acf($img,'slider',array('class'=>'slide__img')) ?><?php endif ?>
					<?php if ($title = get_sub_field('title')): ?>
						<div class="slide__title-container">
							<div class="slide__titles">
								<span class="slide__title"><?php echo $title ?></span>
								<?php if ($subtitle = get_sub_field('subheader')): ?>
								<span class="slide__subtitle"><?php echo $subtitle ?></span>
								<?php endif ?>
							</div>
							<?php if (get_sub_field('link')): ?>

							<div class="slide__link-container">
								<span class="slide__link"><?php echo get_sub_field('link_title') ? get_sub_field('link_title') : _('Read More') ?></span>
							</div>

							<?php endif ?>
						</div>
					<?php endif ?>
				</a>
			</div>
		<?php endwhile ?>

	<?php endif ?>

</div>