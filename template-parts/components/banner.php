<?php if ($img = get_field('banner')): ?>
	<div class="banner" role="banner">
		<?php if (!is_string($img)): ?>
			<?php echo get_image_acf($img,'banner-internal') ?>
		<?php else: ?>
			<div class="banner__cover" style="background-image:url('<?php echo $img ?>');"></div>
		<?php endif ?>
	</div>
<?php endif ?>