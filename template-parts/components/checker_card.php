<?php
?>

<div class="checker-card">

	<?php if(get_sub_field('repeater')): ?>
		<?php $count = 0; while(have_rows('repeater')): the_row(); ?>

			<?php 
				$link = get_sub_field('link') ? get_sub_field('link') : ""; 
				$img = get_sub_field('image');
				$background_style = $img ? "style=\"background-image:url('" . $img . "')\"" : '';
				$count = $count > 3 ? $count = 0 : $count;
			?>

			<div class="checker-card__column checker-card__column-style-<?php echo $count ?>">
				<?php if ($count == 1): ?>

					<div class="checker-card__column-box checker-card__column-box-image" >
						<div class="checker-card__zoom">
							<a class="checker-card__link checker-card__link-zoom" <?php echo $background_style ?> href="<?php echo $link ?>"><span class="sr-only d-none"><?php echo $title ?></span></a>
						</div>
					</div>

					<div class="checker-card__column-box checker-card__column-box-solid">
						<a class="checker-card__link" href="<?php echo $link ?>">
							<?php if ($title = get_sub_field('title')): ?>
							<h2 class="checker-card__header"><?php echo $title ?></h2>
							<?php endif ?>
						</a>
					</div>

				<?php else: ?>

					<div class="checker-card__column-box checker-card__column-box-solid">
						<a class="checker-card__link" href="<?php echo $link ?>">
							<?php if ($title = get_sub_field('title')): ?>
							<h2 class="checker-card__header"><?php echo $title ?></h2>
							<?php endif ?>
						</a>
					</div>

					<div class="checker-card__column-box checker-card__column-box-image" >
						<div class="checker-card__zoom">
							<a class="checker-card__link checker-card__link-zoom" <?php echo $background_style ?> href="<?php echo $link ?>"><span class="sr-only d-none"><?php echo $title ?></span></a>
						</div>
					</div>



				<?php endif ?>

			</div>

		<?php $count++; endwhile ?>
	<?php endif ?>

</div>