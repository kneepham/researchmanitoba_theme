<?php

$counter = 1;

if( have_rows('sections')){

    while ( have_rows('sections') ) : the_row();

        $path = 'template-parts/components/' . get_row_layout();

        ?>
        <div id="section-<?php echo $counter ?>-<?php echo get_row_layout() ?>" class="section"><?php get_template_part($path); ?></div>
        <?php 

        $counter++;

    endwhile;

}