<?php

$category_id = get_field('category');

$args = array(
	'post_type' 		=> 'magazine',
	'posts_per_page'	=>	-1,
	'meta_key' 			=> 'upload_date',
	'orderby'			=> 'upload_date',
	'order'				=> 'DESC',
	'post__not_in' => get_excluded_magazine_post('get'),
	'tax_query' => array(
		array(
			'taxonomy' => 'magazine_category',
			'field'    => 'id',
			'terms'    => $category_id,
		),
	)
);

$magazine_query = new WP_Query($args);
$read_more = get_field('read_more');

$years = array();

?>

<div class="magazine__past-issues">

	<?php while($magazine_query->have_posts()): $magazine_query->the_post() ?>

		<?php ob_start() ?>
		<?php $link = get_field(get_field('link_type')) ?>
		<a class="magazine__list-item-link" href="<?php echo $link ?>"><?php echo get_field('issue_title_date') ? get_field('issue_title_date') . ': ' : $read_more . ': ' ?> <?php echo get_the_title() ?></a>
		<?php $magazine = ob_get_clean() ?>

		<?php $years[date('Y',strtotime(get_field('upload_date')))][] = $magazine ?>

	<?php endwhile; wp_reset_postdata(); ?>

	<?php foreach($years as $year => $items): ?>
		<div class="magazine__column <?php echo $year ?>">
			<h2 class="magazine__year-header"><?php echo $year ?></h2>
			<ul class="magazine__list">
				<?php foreach($items as $item): ?>
					<li class="magazine__list-item"><?php echo $item ?></li>
				<?php endforeach ?>
			</ul>
		</div>
	<?php endforeach ?>

</div>