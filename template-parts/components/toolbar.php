<div class="toolbar" role="search">
	<div class="container-main">

		<div class="toolbar__items">

			<div class="toolbar__social-media">

				<?php if (get_field('social_media_repeater','options')): ?>
					<?php while(have_rows('social_media_repeater','options')): the_row(); ?>

						<?php if (get_sub_field('link')): ?>
							<div class="toolbar__social-media-item">
								<a class="toolbar__social-media-item-link" href="<?php echo get_sub_field('link') ?>">
									<i class="<?php echo get_sub_field('icons') ?> toolbar__social-media-icon"></i>
								</a>
							</div>
						<?php endif ?>
					<?php endwhile ?>
				<?php endif ?>

			</div>

			<div class="toolbar__search">

				<form class="toolbar__search-form"  action="/" method="get">

					<div class="toolbar__search-input-container">
						<label class="visuallyhidden sr-only d-none"><?php echo ('Searchbar') ?></label>
						<input aria-label="Search" class="toolbar__search-input" name="s" type="text" placeholder="<?php echo _('SEARCH') ?>" value="">
					</div>

					<div class="toolbar__search-button-container">
						<button aria="button" class="toolbar__search-button" type="submit" ><i class="fas fa-search toolbar__search-button-icon"></i>
							<span class="sr-only d-none"><?php echo ('Search ') ?></span>
						</button>
					</div>

				</form>

			</div>

		</div>

	</div>
</div>