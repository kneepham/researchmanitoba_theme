<div class="social-media__footer" style="display: flex; color:#415a68;">
	<?php if (get_field('social_media_repeater','options')): ?>
		<?php while(have_rows('social_media_repeater','options')): the_row(); ?>

			<?php if (get_sub_field('link')): ?>
				<div class="toolbar__social-media-item">
					<a class="toolbar__social-media-item-link" href="<?php echo get_sub_field('link') ?>">
						<i class="<?php echo get_sub_field('icons') ?> toolbar__social-media-icon"></i>
					</a>
				</div>
			<?php endif ?>
		<?php endwhile ?>
	<?php endif ?>
</div>
