<?php
	$img = get_sub_field('image');
	$background_style = ($img && isset($img['url']) ? "style=\"background-image:url('" . $img['url'] . "')\"" : '');

	$link = get_sub_field(get_sub_field('link_type'));
?>

<div class="large-card" <?php echo $background_style ?>>

	<div class="large-card">
		<div class="large-card__info-box">

			<?php if ($title = get_sub_field('title')): ?>
			<h2 class="large-card__header"><?php echo $title ?></h2>
			<?php endif ?>

			<?php if ($summary = get_sub_field('summary')): ?>
			<p class="large-card__summary"><?php echo $summary ?> </p>
			<?php endif ?>

			<?php if ($link): ?>
			<div class="large-card__link-container">
				<a class="large-card__link" href="<?php echo $link ?>"><?php echo get_sub_field('button_title') ? get_sub_field('button_title') : "Learn More" ?></a>
			</div>
			<?php endif ?>

		</div>
	</div>

</div>