<div class="drawer">
	<div class="drawer__before">

        <span class="drawer__logo">
            <a href="<?php echo get_site_url() ?>">
                <span class="sr-only d-none"><?php echo ('Returns to Home page - ') . get_bloginfo('name') ?></span>
                <?php if ($logo = get_field('logo','options')): ?>
                    <img src="<?php echo isset($logo['url']) ? $logo['url'] : '' ?>">
                <?php else: ?>
                    <img src="/wp-content/uploads/2018/11/logo_new.jpg"/>
                <?php endif ?>
            </a>
        </span>

        <div class="drawer__book">
            
        </div> 

	</div>
    <nav class="drawer__navigation" role="navigation">

        <?php
            $primary_menu = wp_nav_menu( array(
                // 'theme_location' => 'mobile',
                'theme_location' => 'primary',
                'menu_id'        => 'mobile',
                'container' => '',
                'depth' => 2,
                'echo' => false,
                'menu_class' => 'drawer__navigation-ul'
            ) );
            echo $primary_menu;
        ?>

    </nav>
	<div class="drawer__after">

        <div class="drawer__side">
            <?php
                $navigation = wp_nav_menu( array(
                    'theme_location' => 'sidebar',
                    'menu_id'        => 'sidebar2',
                    'container' => '',
                    'echo' => false
                ) );

                echo $navigation;
            ?>
        </div>

        <div class="drawer__links">

            <div class="drawer__phone">

                <div class="drawer__content-data">
                    <a href="tel:<?php _e('204-775-1096') ?>"><span><?php _e('Telephone: ') ?></span><?php _e('204-775-1096') ?></a>
                </div>

                <div class="drawer__content-data">
                    <a href="tel:<?php _e('204-786-5401') ?>"><span><?php _e('Fax: ') ?></span><?php _e('204-786-5401') ?></a>
                </div>

            </div>

        </div>

        <div class="drawer__social-media">
            <?php if (get_field('social_media','options')): ?>
                <?php while(have_rows('social_media','options')): the_row(); ?>
                    <div class="drawer__social-media-item">
                        <a class="drawer__social-media-item-link" href="<?php echo get_sub_field('link') ?>"><i class="fab fa-<?php echo get_sub_field('icons') ?> drawer__social-media-icon"></i></a>
                    </div>
                <?php endwhile ?>
            <?php endif ?>
        </div>

	</div>
</div>

<div class="drawer__tint"></div>