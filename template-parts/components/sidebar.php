<aside id="side">
	<nav class="sidebar__navigation" role="navigation">
		<?php
			$navigation = wp_nav_menu( array(
				'theme_location' => 'sidebar',
				'menu_id'        => 'sidebar',
				'container' => '',
				'echo' => false
			) );

			echo $navigation;
		?>
	</nav>

	<h1 class="twitter_header"><?php echo _('Twitter Feed') ?></h1>
	<div class="twitter_feed">
<a class="twitter-timeline" data-height="550" href="https://twitter.com/Research_MB?ref_src=twsrc%5Etfw">Tweets by Research_MB</a> <script async src="https://platform.twitter.com/widgets.js" charset="utf-8"></script>
</div>
</aside>