'use strict';

var gulp = require('gulp'),
	sass = require('gulp-sass'),
 	livereload = require('gulp-livereload'),
	concat = require('gulp-concat'),
	rename = require('gulp-rename'),
    flatten = require('gulp-flatten'),
	uglify = require('gulp-uglify'); 	

//SASS
var cssFiles = './sass/*.scss',
    cssDest = './';

gulp.task('sass', function () {
  return gulp.src([
        './style-main.css',
        // '!sass/frameworks/bootstrap/*.scss',
        './node_modules/slick-carousel/slick/slick.css',
        // './node_modules/@fancyapps/fancybox/dist/jquery.fancybox.css',
        cssFiles
    ])
    .pipe(sass().on('error', sass.logError))
    .pipe(sass({outputStyle: 'expanded'}).on('error', sass.logError)) //:nested :compact :expanded :compressed
    .pipe(concat('style.css'))
    .pipe(gulp.dest(cssDest))
    .pipe(livereload());
});

//JS
var jsFiles = 'js/src/*.js',
    jsDest = 'js/dist/';

gulp.task('scripts', function() {
    return gulp.src([
            './node_modules/slick-carousel/slick/slick.js',
            // './node_modules/@fancyapps/fancybox/dist/jquery.fancybox.min.js',
            jsFiles,
        ])
        .pipe(concat('scripts.js'))
        .pipe(gulp.dest(jsDest));
});

gulp.task('listen', function () {
    livereload.listen();
    gulp.watch('./sass/**/*.scss', ['sass']);
    gulp.watch('./js/src/*.js', ['scripts']);
});
