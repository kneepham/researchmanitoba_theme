/*! http://responsiveslides.com v1.54 by @viljamis */
(function(c,I,B){c.fn.responsiveSlides=function(l){var a=c.extend({auto:!0,speed:500,timeout:4E3,pager:!1,nav:!1,random:!1,pause:!1,pauseControls:!0,prevText:"Previous",nextText:"Next",maxwidth:"",navContainer:"",manualControls:"",namespace:"rslides",before:c.noop,after:c.noop},l);return this.each(function(){B++;var f=c(this),s,r,t,m,p,q,n=0,e=f.children(),C=e.size(),h=parseFloat(a.speed),D=parseFloat(a.timeout),u=parseFloat(a.maxwidth),g=a.namespace,d=g+B,E=g+"_nav "+d+"_nav",v=g+"_here",j=d+"_on",
w=d+"_s",k=c("<ul class='"+g+"_tabs "+d+"_tabs' />"),x={"float":"left",position:"relative",opacity:1,zIndex:2},y={"float":"none",position:"absolute",opacity:0,zIndex:1},F=function(){var b=(document.body||document.documentElement).style,a="transition";if("string"===typeof b[a])return!0;s=["Moz","Webkit","Khtml","O","ms"];var a=a.charAt(0).toUpperCase()+a.substr(1),c;for(c=0;c<s.length;c++)if("string"===typeof b[s[c]+a])return!0;return!1}(),z=function(b){a.before(b);F?(e.removeClass(j).css(y).eq(b).addClass(j).css(x),
n=b,setTimeout(function(){a.after(b)},h)):e.stop().fadeOut(h,function(){c(this).removeClass(j).css(y).css("opacity",1)}).eq(b).fadeIn(h,function(){c(this).addClass(j).css(x);a.after(b);n=b})};a.random&&(e.sort(function(){return Math.round(Math.random())-0.5}),f.empty().append(e));e.each(function(a){this.id=w+a});f.addClass(g+" "+d);l&&l.maxwidth&&f.css("max-width",u);e.hide().css(y).eq(0).addClass(j).css(x).show();F&&e.show().css({"-webkit-transition":"opacity "+h+"ms ease-in-out","-moz-transition":"opacity "+
h+"ms ease-in-out","-o-transition":"opacity "+h+"ms ease-in-out",transition:"opacity "+h+"ms ease-in-out"});if(1<e.size()){if(D<h+100)return;if(a.pager&&!a.manualControls){var A=[];e.each(function(a){a+=1;A+="<li><a href='#' class='"+w+a+"'>"+a+"</a></li>"});k.append(A);l.navContainer?c(a.navContainer).append(k):f.after(k)}a.manualControls&&(k=c(a.manualControls),k.addClass(g+"_tabs "+d+"_tabs"));(a.pager||a.manualControls)&&k.find("li").each(function(a){c(this).addClass(w+(a+1))});if(a.pager||a.manualControls)q=
k.find("a"),r=function(a){q.closest("li").removeClass(v).eq(a).addClass(v)};a.auto&&(t=function(){p=setInterval(function(){e.stop(!0,!0);var b=n+1<C?n+1:0;(a.pager||a.manualControls)&&r(b);z(b)},D)},t());m=function(){a.auto&&(clearInterval(p),t())};a.pause&&f.hover(function(){clearInterval(p)},function(){m()});if(a.pager||a.manualControls)q.bind("click",function(b){b.preventDefault();a.pauseControls||m();b=q.index(this);n===b||c("."+j).queue("fx").length||(r(b),z(b))}).eq(0).closest("li").addClass(v),
a.pauseControls&&q.hover(function(){clearInterval(p)},function(){m()});if(a.nav){g="<a href='#' class='"+E+" prev'>"+a.prevText+"</a><a href='#' class='"+E+" next'>"+a.nextText+"</a>";l.navContainer?c(a.navContainer).append(g):f.after(g);var d=c("."+d+"_nav"),G=d.filter(".prev");d.bind("click",function(b){b.preventDefault();b=c("."+j);if(!b.queue("fx").length){var d=e.index(b);b=d-1;d=d+1<C?n+1:0;z(c(this)[0]===G[0]?b:d);if(a.pager||a.manualControls)r(c(this)[0]===G[0]?b:d);a.pauseControls||m()}});
a.pauseControls&&d.hover(function(){clearInterval(p)},function(){m()})}}if("undefined"===typeof document.body.style.maxWidth&&l.maxwidth){var H=function(){f.css("width","100%");f.width()>u&&f.css("width",u)};H();c(I).bind("resize",function(){H()})}})}})(jQuery,this,0);



;(function ($, window, document, undefined) {

	$(document).ready(function() {

		$('li.menu-item, ul.dropdown').mouseenter(function() {
			$(this).siblings().removeClass('active');
			$(this).addClass('active');
		}).mouseleave(function() {
			if (!$(this).closest('ul').hasClass('active')) {
				$('li.menu-item, ul.dropdown').removeClass('active');
			} else {
				$(this).children().removeClass('active');
			}
		});
	});

}(jQuery, this, this.document));


// JavaScript Document

function getXMLHTTP() {
	var xmlhttp=false;
	try{
		xmlhttp=new XMLHttpRequest();
	}
	catch(e)	{
		try{
			xmlhttp= new ActiveXObject("Microsoft.XMLHTTP");
		}
		catch(e){
			try{
			xmlhttp = new ActiveXObject("Msxml2.XMLHTTP");
			}
			catch(e1){
				xmlhttp=false;
			}
		}
	}

	return xmlhttp;
}

//
AIM = {

	frame : function(c) {

		var n = 'f' + Math.floor(Math.random() * 99999);
		var d = document.createElement('DIV');
		d.innerHTML = '<iframe style="display:none" src="about:blank" id="'+n+'" name="'+n+'" onload="AIM.loaded(\''+n+'\')"></iframe>';
		document.body.appendChild(d);

		var i = document.getElementById(n);
		if (c && typeof(c.onComplete) == 'function') {
			i.onComplete = c.onComplete;
		}

		return n;
	},

	form : function(f, name) {
		f.setAttribute('target', name);
	},

	submit : function(f, c) {
		AIM.form(f, AIM.frame(c));
		if (c && typeof(c.onStart) == 'function') {
			return c.onStart();
		} else {
			return true;
		}
	},

	loaded : function(id) {
		var i = document.getElementById(id);
		if (i.contentDocument) {
			var d = i.contentDocument;
		} else if (i.contentWindow) {
			var d = i.contentWindow.document;
		} else {
			var d = window.frames[id].document;
		}

		if (d.location.href == "about:blank") {
			return;
		}

		if (typeof(i.onComplete) == 'function') {
			i.onComplete(d.body.innerHTML);
		}
	}

}
//

function ask_start() {
}

function ask_complete(response) {
	var ask_anything = document.getElementById('ask_anything');
	ask_anything.innerHTML = '<div class="ask_anything_result">Thanks, we\'ve received your question and will contact you with a response.</div>';
}

function input_focus(input_box) {
	if (input_box.value == input_box.defaultValue) {
		input_box.value = '';
		input_box.style.fontStyle = 'normal';
		input_box.style.fontSize = '11px';
	}
}

function input_blur(input_box) {
	if (input_box.value === '') {
		input_box.value = input_box.defaultValue;
		//input_box.style.textDecoration = 'oblique';
		input_box.style.fontStyle = 'oblique';
		input_box.style.fontSize = '10px';
	}
}

function menu_drop(ele_name) {
	var menu_id = document.getElementById(ele_name);
	menu_id.style.display = 'block';
}

function menu_hide(ele_name) {
	var menu_id = document.getElementById(ele_name);
	menu_id.style.display = 'none';
}

function colour_glider() {
	// var main_nav = document.getElementById('main_nav');

	// var loop_countr = 0;

	// setInterval(function () {
	// 	main_nav.style.backgroundPosition = loop_countr+'px 0px';
	// 	loop_countr++;
	// }, 50);
}

function printpage() {
	window.print();
}

$(function(){
	$('.twitter_feed ul li:last').addClass('twitlast');
});

!function(d,s,id){var js,fjs=d.getElementsByTagName(s)[0];if(!d.getElementById(id)){js=d.createElement(s);js.id=id;js.src="https://platform.twitter.com/widgets.js";fjs.parentNode.insertBefore(js,fjs);}}(document,"script","twitter-wjs");

$(document).ready(function() {



	$('#askanything-submit').click(function() {
		valid = true;
		defaultColor = '#415A68';
		$('#ask_anything li, #ask_anything .input, #ask_anything label').css('color', defaultColor);

		if ($('#askanything-name input').val().length <= 0) {
			$('#askanything-name label').css('color', '#ff0000');
			valid = false;
		}
		if ($('#askanything-email input').val().length <= 0) {
			$('#askanything-email label').css('color', '#ff0000');
			valid = false;
		}
		if ($('#askanything-ask textarea').val().length <= 0) {
			$('#askanything-ask label').css('color', '#ff0000');
			valid = false;
		}
		if ( ! $('#aa_general').is(':checked') && ! $('#aa_help_desk').is(':checked')) {
			$('#askanything-options li').css('color', '#ff0000');
			valid = false;
		}
		email_address = $('#askanything-email input');
		email_regex = /^[a-zA-Z0-9._-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i;

		if(!email_regex.test(email_address.val())) {
			$('#askanything-email .error-email').html('- Please enter a valid email address.');
 			valid = false;
		} else {
			$('#askanything-email .error-email').html('');
		}

		if (valid == false) {
			$('#ask_anything .error-form').html('All fields are required.');
			return false;
		}

		return true;
	});



	/*
	RESPONSIVE SLIDES
	****/
	$(".rslides").responsiveSlides({
		auto: true,
		speed: 500,
		timeout: 4000,
	});


	/*
	MEMBERS SECTION
	****/


	$('.file-comments-link').on('click', function(e){
		e.preventDefault();
		$(this).parent().toggleClass('open').next().slideToggle('fast');
	});

	var loginSubmit = $('#wpmem_login input[type=submit]').val();
	if ( loginSubmit == 'Update Password' ) {
		$('#wpmem_login input[type=submit]').val('Update   →');
	} else {
		$('#wpmem_login input[type=submit]').val('Login   →');
	}
	
	var regSubmit = $('#wpmem_reg input[type=submit]').val();
	if ( regSubmit == 'Update Profile' ) {
		$('#wpmem_reg input[type=submit]').val('Update   →');
	} else {
		$('#wpmem_reg input[type=submit]').val('Register   →');
	}

	$('.file-title a').on('click', function(e){
		e.preventDefault();

		var
			$this = $(this),
			$target = $this.parent().next();


		$('.file-description').slideUp(200);

		if( !$this.hasClass('active') ) {
			$this.addClass('active');
			$target.slideDown(200);
		} else {
			$this.removeClass('active');
			$target.slideUp(200);
		}

		
	});

	$('#wpmem_login .link-text a').text('click here');

});