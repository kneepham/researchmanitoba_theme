<?php
/**
 * Template Name: FORWARD
 */

if ( have_posts() ) {
	while ( have_posts() ) {
		the_post();
	}
}

$id = get_the_ID();
$pages = get_posts('numberposts=500&orderby=menu_order&post_type=page&post_parent='.$id);

header('Location: '.home_url( '/' ).'?page_id='.$pages[0]->ID);
?>
