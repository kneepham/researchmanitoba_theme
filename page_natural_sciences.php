<?php
/**
 * Template Name: Natural Sciences (Landing Page)
 */
get_header();

if ( have_posts() ) {
	while ( have_posts() ) {
		the_post();
	}
}

$the_parent = $post->post_parent;
/* Hardcoded to show Programs Page sidebar */
$id = 42;
//get_the_ID();

if ($the_parent == 0) {
	$the_parent = $id;
}

$title = get_the_title();
$content = get_the_content();

$ancestors = get_post_ancestors( $id );
$ancestor_id = end($ancestors);

$template_colour = get_post_meta($ancestor_id, 'template_colour', true);
if (!$template_colour) {
	$template_colour = 'purple';
}

$banner = get_post_meta($id, 'banner', true);
if (!$banner) {
	$banner = get_post_meta($ancestor_id, 'banner', true);
}
if( substr( $banner, 0, 4 ) === "http" ) {
    $banner_out = $banner;
} else {
    $banner_out = get_bloginfo('url') . $banner;
}

$sidebar = get_post_meta($id, 'sidebar', true);
$sidebar_am = $wpdb->get_results("SELECT * FROM $wpdb->postmeta WHERE meta_key='sidebar' AND post_id=".$id." ORDER BY meta_id ASC");

$pages = get_posts('numberposts=500&orderby=menu_order&order=ASC&post_type=page&post_parent='.$id);

?>

				<div class="main_content">
                	

                	<div class="holder <?php echo $template_colour; ?>">
                    	<div class="copy_content">
                        	<h1><?php echo $title; ?></h1>

                            <ul class="programs">
                            	<?php //foreach ($pages as $key => $value) : ?>
                                <li class="default">
                                    <?php echo wpautop($content); //echo $pages[0]->post_content; ?>
                                </li>
                                <?php //endforeach; ?>
                            </ul>
                        </div>

                        <aside>

                        	<?php
								//foreach ($sidebar as $key => $value) :
								foreach ($sidebar_am as $key => $value) :
									//$sidebar_data = explode('|', $value);
									$sidebar_data = explode('|', $value->meta_value);
									$scount = count($sidebar_data);

									if ($scount == 1) :
										echo '<div class="sidebar-item">' . $sidebar_data[0] . '</div>';
									elseif ($scount == 2) :
							?>

                            	<a href="<?php echo $sidebar_data[1]; ?>" class="sidebar_img_px1" target="_blank">
                                    <img src="<?php echo $sidebar_data[0]; ?>">
                                </a>

                            <?php elseif ($scount == 3 || $scount == 4) : ?>

                                <a href="<?php echo $sidebar_data[2]; ?>" class="sidebar_img_px2" target="_blank">
                                    <img src="<?php echo $sidebar_data[1]; ?>">

                                    <h1><?php echo $sidebar_data[0]; ?></h1>

                                    <?php if ($sidebar_data[3]) : ?>
                                    <h2><?php echo $sidebar_data[3]; ?></h2>
                                    <?php endif; ?>
                                </a>

                           <?php
						   		   endif;
								endforeach;

								if ($sidebar_inc) {
									include $sidebar_inc;
								}
							?>

                            <?php 
                            $defaults = array(
                                'theme_location'  => 'natural-sciences-and-engineering-research',
                            );
                             
                            wp_nav_menu( $defaults );
                            ?>

                            
                        </aside>
                    </div>
                </div>

                <div class="clear_both"></div>

<?php get_footer(); ?>
