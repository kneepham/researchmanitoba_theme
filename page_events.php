<?php
/**
 * Template Name: Events
 */
get_header(); 
 
if ( have_posts() ) {
	while ( have_posts() ) {
		the_post();
	}
}

$the_parent = $post->post_parent;
$id = get_the_ID();

$title = get_the_title();
$content = get_the_content();

$ancestors = get_post_ancestors( $id );
$ancestor_id = end($ancestors);

$template_colour = get_post_meta($ancestor_id, 'template_colour', true);
if (!$template_colour) {
	$template_colour = 'purple';
}

$banner = get_post_meta($id, 'banner', true);
if (!$banner) {
    $banner = get_post_meta($ancestor_id, 'banner', true);
}
if( substr( $banner, 0, 4 ) === "http" ) {
    $banner_out = $banner;
} else {
    $banner_out = get_bloginfo('url') . $banner;
}

$sidebar = get_post_meta($the_parent, 'sidebar', true);

$pages = get_posts('numberposts=500&orderby=menu_order&post_type=page&post_parent='.PAGE_EVENTS_CAL);

?>

				<div class="main_content">
                	
                    
                	<div class="holder <?php echo $template_colour; ?>">
                    	<div class="copy_content">
                        	<h1>Events</h1>
                            <h2 class="events"><?php echo $title; ?></h2>
                            
                            <p><?php echo $content; ?></p>
                            
                            <ul class="events_gallery">
                            	<li><a class="fancybox" rel="event_gal" href="big_image_1.jpg"><img src="small_image_1.jpg" alt="" /></a></li>
                                <li><a class="fancybox" rel="event_gal" href="big_image_1.jpg"><img src="small_image_1.jpg" alt="" /></a></li>
                                <li><a class="fancybox" rel="event_gal" href="big_image_1.jpg"><img src="small_image_1.jpg" alt="" /></a></li>
                                <li><a class="fancybox" rel="event_gal" href="big_image_1.jpg"><img src="small_image_1.jpg" alt="" /></a></li>
                            </ul>
                        </div>
                        
                        <aside>
                        
                        	<?php 
								//foreach ($sidebar as $key => $value) : 
								foreach ($sidebar_am as $key => $value) : 
									//$sidebar_data = explode('|', $value);
									$sidebar_data = explode('|', $value->meta_value);
									$scount = count($sidebar_data);
									
									if ($scount == 1) :
										echo '<div class="sidebar-item">' . $sidebar_data[0] . '</div>';
									elseif ($scount == 2) : 
							?>
                            
                            	<a href="<?php echo $sidebar_data[1]; ?>" class="sidebar_img_px1" target="_blank">
                                    <img src="<?php echo $sidebar_data[0]; ?>">                                    
                                </a>
                                
                            <?php elseif ($scount == 3 || $scount == 4) : ?>
                                
                                <a href="<?php echo $sidebar_data[2]; ?>" class="sidebar_img_px2" target="_blank">
                                    <img src="<?php echo $sidebar_data[1]; ?>">                                    
                                    
                                    <h1><?php echo $sidebar_data[0]; ?></h1>
                                    
                                    <?php if ($sidebar_data[3]) : ?>
                                    <h2><?php echo $sidebar_data[3]; ?></h2>
                                    <?php endif; ?>
                                </a>
                                
                           <?php      
						   		   endif;	
								endforeach; 
								
								if ($sidebar_inc) {
									include $sidebar_inc;
								}
							?>
                                                                               
                            <ul>
                            	<?php foreach ($pages as $key => $value) : ?>
                                <li><a href="<?php echo get_permalink($value->ID); ?>"><?php echo $value->post_title; ?></a></li>
                                <?php endforeach; ?>
                            </ul>
                            
                        </aside>
                    </div>
                </div>
                
                <div class="clear_both"></div>
                
<?php get_footer(); ?>
