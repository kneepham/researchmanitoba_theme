<?php
/**
 * Template Name: Magazine
 */
?>

<?php get_header('front'); ?>

<div class="container-main">
    <?php while ( have_posts() ): the_post(); ?>
        <?php get_template_part( 'template-parts/components/banner' ); ?>
    <?php endwhile ?>
</div>

<div class="container-main container-main__flex">

    <div class="container-main__left">
        <?php get_template_part( 'template-parts/components/sidebar' ); ?>
    </div>

    <div class="container-main__right">
        <?php while ( have_posts() ): the_post(); ?>
        	<div class="content magazine__content">
                <h1 class="magazine__header-1 <?php echo get_field('banner') ? 'has-banner' : '' ?>"><?php echo get_the_title() ?></h1>
        		<?php the_content() ?>
        	</div>
            <?php get_template_part( 'template-parts/components/section-loop' ); ?>
            <?php get_template_part( 'template-parts/components/magazine-featured' ); ?>
            <?php get_template_part( 'template-parts/components/magazine-past-issues' ); ?>
        <?php endwhile ?>
    </div>

</div>

<?php get_footer('front'); ?>
