<?php
/**
 * The template for displaying 404 pages (Not Found).
 *
 * @package WordPress
 * @subpackage blankSlate
 * @since blankSlate 3.1
 */
$the_parent = $post->post_parent;
$id = get_the_ID();

if ($the_parent == 0) {
	$the_parent = $id;
}

$title = get_the_title();
$content = do_shortcode( get_the_content() );

$ancestors = get_post_ancestors( $id );
$ancestor_id = end($ancestors);

$banner = get_post_meta($id, 'banner', true);
if (!$banner) {
	$banner = get_post_meta($ancestor_id, 'banner', true);
}

get_header(); ?>

<div class="main_content">
                	<img src="<?php bloginfo('template_url'); ?>/template/img/blog.jpg" class="sub_banner">
                    
                	<div class="holder blue">
                    	<div class="copy_content">
			
							<h1><?php _e( 'Not Found', 'twentyten' ); ?></h1>
							
							<p><?php _e( 'Apologies, but the page you requested could not be found. Perhaps searching will help.', 'twentyten' ); ?></p>
							
							<?php get_search_form(); ?>
                    	</div>

                    	<?php if( is_user_logged_in() ) { ?>

	                    <aside>

	                            <?php
	                                $taxonomy = 'wpdmcategory';
	                                $tax_terms = get_terms($taxonomy);
	                            ?>
	                            <ul class="file-categories">
	                                <?php
	                                    $current_url = 'http'.(empty($_SERVER['HTTPS'])?'':'s').'://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
	                                    foreach ($tax_terms as $tax_term) {
	                                        $page_url = get_bloginfo('url') . '/council-login/' . $tax_term->slug . '/';

	                                        if( $page_url == $current_url ) {
	                                            $class = 'class="active"';
	                                        } else {
	                                            $class = '';
	                                        }
	                                        ?>
	                                        <li>
	                                            <a <?php echo $class; ?> href="<?php echo $page_url; ?>"><?php echo $tax_term->name; ?></a>
	                                        </li>
	                                    <?php } ?>
	                            </ul>

	                        </aside>

	                  <?php } ?>


                    </div>

                    
</div>
				

	<script type="text/javascript">
		// focus on search field after it has loaded
		document.getElementById('s') && document.getElementById('s').focus();
	</script>

<?php get_footer(); ?>