<?php

require get_template_directory() . '/inc/custom-post-types.php';
require get_template_directory() . '/inc/theme.php';
require get_template_directory() . '/inc/helpers.php';
require get_template_directory() . '/inc/acf-hooks.php';