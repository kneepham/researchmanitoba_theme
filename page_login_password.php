<?php
/**
 * Template Name: Login (Password)
 */
get_header(); 

if ( have_posts() ) {
	while ( have_posts() ) {
		the_post();
	}
}

$the_parent = $post->post_parent;
$id = get_the_ID();

if (isset($_REQUEST['login'])) {
	$logins = get_post_meta(PAGE_LOGIN, 'login', false);

	$matches = 0;
	
	$return_r = 'fail';
	
	foreach ($logins as $key => $value) {
		$user_pass = explode('|', $value);
		
		if ($_REQUEST['email'] == $user_pass[2]) {
			$to = $user_pass[2];
			$subject = 'MHRC - Council Login - Password';
			
			$message = "Hello, \r\n \r\n
Your password was requested for the Manitoba Health Research Council Login. Your account details are: \r\n\r\n
			
Username: ".$user_pass[0]." \r\n
Password: ".$user_pass[1]." \r\n\r\n
			
Regards, \r\n
Manitoba Health Research Council";
			
			$headers = "From: noreply@researchmanitoba.ca\r\n";
			
			mail($to, $subject, $message, $headers);
			
			$return_r = $user_pass[2];
			break;	
		}
	}
	
	header('Location: '.home_url( '/' ).'board-login/password-reset/?reset='.urlencode($return_r));
	exit();
}

if ($the_parent == 0) {
	$the_parent = $id;
}

$title = get_the_title();
$content = get_the_content();

$ancestors = get_post_ancestors( $id );
$ancestor_id = end($ancestors);

$template_colour = get_post_meta($ancestor_id, 'template_colour', true);
if (!$template_colour) {
	$template_colour = 'purple';
}

$banner = get_post_meta($id, 'banner', true);
if (!$banner) {
	$banner = get_post_meta($ancestor_id, 'banner', true);
}
if( substr( $banner, 0, 4 ) === "http" ) {
    $banner_out = $banner;
} else {
    $banner_out = get_bloginfo('url') . $banner;
}

$sidebar = get_post_meta($the_parent, 'sidebar', true);

?>

				<div class="main_content">
                	
                    
                	<div class="holder <?php echo $template_colour; ?>">
                    	<div class="copy_content">
                        	<div class="c-login">Board Login</div>

                        	<p>FORGOT YOUR PASSWORD?</p>
                            
                            <div class="cl_form">
                            <?php if (isset($_REQUEST['reset']) && $_REQUEST['reset'] != 'fail') : ?>
                            <span class="login_message">Your password was sent to <em><?php echo $_REQUEST['reset']; ?></em>.</span>
                            <?php elseif (isset($_REQUEST['reset']) && $_REQUEST['reset'] == 'fail') : ?>
                            <span class="login_message">We could not find the email address you entered in our system.<br />Please contact your administrator or enter another email address below.</span>
                            <?php endif; ?>
                            
                            <form action="<?php echo home_url( '/' ); ?>board-login/password-reset/" method="post">
                            	<label class="council_login_label cblue">Enter your email address below and we’ll send you a link to reset your password.</label>
                            	<input name="email" type="text" class="council_login_input cblue" placeholder="Email Address" />
                                <input name="login" type="submit" name="Submit" value="Send Reset Link" class="buttons">
                            </form>
                            </div>
                            
							<?php //echo $content; ?>
                        </div>
<?php /*                        
                        <aside>
                        	<?php echo $sidebar; ?>
                        </aside> */ ?>
                    </div>
                </div>
                
                <div class="clear_both"></div>
                
<?php get_footer(); ?>
