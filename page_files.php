<?php
/**
 * Template Name: Council Files
 */
get_header();

global $post;
$slug = get_post( $post )->post_name;

 
if ( have_posts() ) {
	while ( have_posts() ) {
		the_post();
	}
}

$the_parent = $post->post_parent;
$id = get_the_ID();

if ($the_parent == 0) {
	$the_parent = $id;
}

$title = get_the_title();
$content = get_the_content();

$ancestors = get_post_ancestors( $id );
$ancestor_id = end($ancestors);


$banner = get_post_meta($id, 'banner', true);
if (!$banner) {
    $banner = get_post_meta($ancestor_id, 'banner', true);
}
if( substr( $banner, 0, 4 ) === "http" ) {
    $banner_out = $banner;
} else {
    $banner_out = get_bloginfo('url') . $banner;
}




$file_query_args = array(
    'post_type' => 'wpdmpro',
    'tax_query' => array(
        array(
            'taxonomy' => 'wpdmcategory',
            'field'    => 'slug',
            'terms'    => $slug,
        ),
    ),
);
$file_query = new WP_Query( $file_query_args );

?>

				<div class="main_content">
                	
                    
                	<div class="holder blue">
                    	<div class="copy_content">
                            <h1>Member Section</h1>
                        	<h2 class="file-category-title"><?php echo $title; ?></h2>

                            <?php
                            if ( $file_query->have_posts() ) {
                                while ( $file_query->have_posts() ) {
                                    $file_query->the_post();

                                    $num_comments = get_comments_number();
                                    if ( $num_comments == 0 ) {
                                        $comments = __('Leave a comment');
                                    } elseif ( $num_comments > 1 ) {
                                        $comments = $num_comments . __(' comments');
                                    } else {
                                        $comments = __('1 comment');
                                    }

                                    ?>

                                    <div class="file-wrap">
                                        <?php echo do_shortcode('[wpdm_package id="' . get_the_ID() . '" template="558988289bbf8"]'); ?>
                                        <a class="file-comments-link" href="#"><?php echo $comments; ?></a>
                                    </div>

                                    <div class="file-comments">
                                        <?php comments_template(); ?>
                                    </div>
                            <?php
                                }
                            }
                            wp_reset_postdata();
                            ?>
                            
                        </div>
                        
                        <aside>

                            <?php
                                $taxonomy = 'wpdmcategory';
                                $tax_args = array(
                                    'orderby' => 'name', 
                                    'order' => 'ASC',
                                );
                                $tax_terms = get_terms($taxonomy, $tax_args);
                            ?>
                            <ul class="file-categories">
                                <?php
                                    $current_url = 'http'.(empty($_SERVER['HTTPS'])?'':'s').'://'.$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
                                    foreach ($tax_terms as $tax_term) {
                                        $page_url = get_bloginfo('url') . '/download-category/' . $tax_term->slug . '/';

                                        if( $page_url == $current_url ) {
                                            $class = 'class="active"';
                                        } else {
                                            $class = '';
                                        }
                                        ?>
                                        <li>
                                            <a <?php echo $class; ?> href="<?php echo $page_url; ?>"><?php echo $tax_term->name; ?></a>
                                        </li>
                                    <?php } ?>
                            </ul>

                        </aside>
                    </div>
                </div>
                
                <div class="clear_both"></div>
                
<?php get_footer(); ?>
