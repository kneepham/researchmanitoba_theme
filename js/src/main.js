(function(){

    if ($('.slick__slider').length)
    {
        $('.slick__slider').each(function(){

            var slick_args = {};

            slick_args['arrows'] = false;
            slick_args['dots'] = $(this).data('pager');
            slick_args['autoplay'] = $(this).data('autoplay');
            slick_args['autoplaySpeed'] = $(this).data('interval');
            slick_args['speed'] = $(this).data('transition-speed');
            slick_args['fade'] = $(this).data('transition-type');
            slick_args['infinite'] = $(this).data('infinite');

            $('.slick__slider').slick(slick_args); 
        });
    }

    if ($('a').length)
    {
        $('a[href*="/"]').each(function() {

            if ($(this).data('fancybox')){
                return true;
            }

            var a = new RegExp('/' + window.location.host + '/'),
                mailto = new RegExp('/mailto:/');

            if(!a.test(this.href)) {
                var span = $('<span></span>');
                span.html('Opens in new window');
                span.addClass('sr-only d-none');
                $(this).prepend(span);

                $(this).click(function(event) {
                   event.preventDefault();
                   event.stopPropagation();
                   window.open(this.href, '_blank');
                });
            }
        });
    }

    //Accessiblity if no alt text use file name
    $('img').each(function(){
        if(!$(this).attr('alt'))
        {
            $(this).attr('alt',$(this).attr('src').split("/").pop())
        }
    });


    // Select the node that will be observed for mutations
    var targetNode = document.getElementById('side');

    // Options for the observer (which mutations to observe)
    var config = { 
        attributes: true,
        characterData: true,
        childList: true,
        subtree: true,
        attributeOldValue: true,
        characterDataOldValue: true
    };

    // Callback function to execute when mutations are observed
    var callback = function(mutationsList, observer) {

        var target = null;

        for(var mutation of mutationsList) {

           if (mutation.target.id.startsWith('twitter-widget-0'))
           {
                target = true;
           }

        }

        if (target)
        {
            $('.twitter-timeline').attr('tabindex','-1');
            observer.disconnect();
        }

    };

    // Create an observer instance linked to the callback function
    var observer = new MutationObserver(callback);

    // Start observing the target node for configured mutations
    observer.observe(targetNode, config);


})();