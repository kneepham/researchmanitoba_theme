(function(){
	
	$(document).ready(function() {

		var hasFlash = false;
		try {
		  var fo = new ActiveXObject('ShockwaveFlash.ShockwaveFlash');
		  if(fo) hasFlash = true;
		}catch(e){
		  if(navigator.mimeTypes ["application/x-shockwave-flash"] != undefined) hasFlash = true;
		}
		

		if (hasFlash) {
			var flashvars = {};
			var params = {};
			params.wmode = "transparent";
			var attributes = {};
			
			swfobject.embedSWF("<?php bloginfo('template_url'); ?>/template/flash/mhrc_anim_final.swf", "logo", "184", "130", "9.0.0", false, flashvars, params, attributes);	
		} else {
			$('#logo').html('<img src="<?php bloginfo("template_url"); ?>/template/img/logo.png" alt="Research Manitoba" />');
		}
		
		$(".fancybox").fancybox();
		colour_glider();
	});
	
	function flash_home_link() {
		window.location = '<?php echo home_url( '/' ); ?>';
	}
	
})();
