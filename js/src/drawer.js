(function(){

    if($('.hamburger').length){

        var $drawer = $('.drawer');
        var tl_drawer = new TimelineLite();

        function hide_drawer(){
            if ( $('body').hasClass('drawer--open'))
            {
                tl_drawer.to($drawer, 0, {'left': '0px', ease: Power4.easeOut})
                .to($drawer, 0.3, {'x': '-100%', ease: Power4.easeOut})
                .to($drawer, 0, {'display':'none'});
                $('.hamburger').removeClass('is-active');
                $('body').removeClass('drawer--open');
            }
        }

        $('.hamburger').click(function(){

            $(this).toggleClass('is-active');

            $('body').toggleClass('drawer--open');

            if ($('.drawer').length)
            {

                if ($(this).hasClass('is-active'))
                {
                    tl_drawer.to($drawer, 0, {'display':'block'})
                    .to($drawer, .5, {'x': '0%', ease: Power4.easeOut});
                }
                else
                {
                    tl_drawer.to($drawer, 0, {'left': '0px', ease: Power4.easeOut})
                    .to($drawer, 0.3, {'x': '-100%', ease: Power4.easeOut})
                    .to($drawer, 0, {'display':'none'});
                }
            }

        });

        $('.drawer__navigation-ul > li.menu-item-has-children > a').each(function(){
            var $span = $('<span><i class="fas fa-chevron-down"></i></span>');
            $span.addClass('menu-item-toggle');
            $(this).append($span);

            $span.click(function(e){
                e.preventDefault();
                // e.stopPropagation();

                var $menu = $(this).closest('li').find('.sub-menu');

                $(this).toggleClass('is-active');

                if ($(this).hasClass('is-active'))
                {
                    var tl_menu = new TimelineLite();
                    tl_menu.to($menu, 0, {'display':'block'})
                    .from($menu, 0.4, {'height':'0px', ease:Cubic.easeInOut});
                }
                else
                {
                    var tl_menu = new TimelineLite();
                    tl_menu.to($menu, 0.4, {'height':'0px', ease:Cubic.easeInOut})
                    .to($menu, 0, {'display':'none'})
                    .to($menu, 0, {'height':'auto'});
                }
            });

        });        

        $('.drawer__tint').click(function(){
            hide_drawer();
        });

        $(window).resize(function(){
            hide_drawer();
        });

    }



})();