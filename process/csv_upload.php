<?php
ini_set("auto_detect_line_endings", true);

$r_year = $_REQUEST['year'];
if (!is_numeric($r_year)) {
	die();
}

if ($_FILES['media']['size'] > 0) {
	//
	include '../../../../wp-config.php';

	$cxn = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME) or die(mysqli_error($cxn));
	
	
	//
	$uploaded_file_name = strtotime('now').'csv';
	
	$file_path = '../../../../csv/'.$uploaded_file_name;
	move_uploaded_file($_FILES['media']['tmp_name'], $file_path);


	//
	$row = 1;
	if (($handle = fopen($file_path, "r")) !== FALSE) {
		$queries = array();
		
		while (($data = fgetcsv($handle, 1000, ",", '"')) !== FALSE) {
			if ($row > 1) {
				$num = count($data);
				
				for ($c = 0; $c < $num; $c++) {
					$data[$c] = mysqli_escape_string($cxn, $data[$c]);
				}
				
				$queries[] = "('".$r_year."', '".$data[0]."', '".$data[1]."', '".$data[2]."', '".$data[3]."', '".$data[4]."', '".$data[5]."', '".$data[6]."', '".$data[7]."', '".$data[8]."')";
			}
			
			$row++;
		}
		 
		fclose($handle);
	}
	
	
	//
	$query = "UPDATE mhrc_recipients SET trashed='y' WHERE r_year=".$r_year;
	$result = mysqli_query($cxn, $query) or die($query.': '.mysqli_error($cxn));
	
	
	//
	if (!empty($queries)) {
		$query_line = implode(', ', $queries);
	
		$query = "INSERT INTO mhrc_recipients (r_year, r_category, r_name, project_title, award_title, writeup, department, faculty, school, photo_url) VALUES ".$query_line;
		$result = mysqli_query($cxn, $query) or die($query.': '.mysqli_error($cxn));
	}
}


$query = "SELECT id, r_year FROM mhrc_recipients WHERE trashed='n'";
$result = mysqli_query($cxn, $query) or die($query.': '.mysqli_error($cxn));
		
$recipients_years = array();

while ($data = mysqli_fetch_object($result)) {
	$recipients_years[$data->r_year][] = $data->id;
}
?>

<li><a href="<?php echo $_REQUEST['template_url']; ?>/process/csv_download.php?y=0"><strong>Download Empty Recipient CSV</strong></a></li>
<?php 
	foreach($recipients_years as $key => $value) : 
		$total_records = count($value);
		
		$year_record = $total_records.' records';
		if ($total_records == 1) {
			$year_record = '1 record';
		}
?>
<li><a href="<?php echo $_REQUEST['template_url']; ?>/process/csv_download.php?y=<?php echo $key; ?>"><?php echo $key; ?> CSV Download (<?php echo $year_record; ?>)</a></li>
    
<?php endforeach; ?>