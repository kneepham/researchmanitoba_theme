<?php
ini_set("auto_detect_line_endings", true);

$r_year = $_REQUEST['y'];
if (!is_numeric($r_year)) {
	die();
}


//
include '../../../../wp-config.php';

$cxn = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME) or die(mysqli_error($cxn));


//
$query = "SELECT * FROM mhrc_recipients WHERE r_year=".$r_year." ORDER BY id";
$result = mysqli_query($cxn, $query) or die($query.': '.mysqli_error($cxn));

$list = array();
$list[] = array('Recipient category', 'Recipient name', 'Project title', 'Award title', 'Write-up', 'Department', 'Faculty', 'School', 'Photo URL');

while ($data = mysqli_fetch_object($result)) {
	$list[] = array($data->r_category, $data->r_name, $data->project_title, $data->award_title, $data->writeup, $data->department, $data->faculty, $data->school, $data->photo_url); 
}


//
$file_name = 'mhrc_recipients_'.$r_year.'DB_'.date('Ymd_gis').'.csv';

if ($r_year == 0) {
	$file_name = 'mhrc_recipientsDB_template.csv';
}

$file_path = '../../../../csv/';

$csv_file = $file_path.$file_name;

$fp = fopen($csv_file, 'w');

foreach ($list as $fields) {
    fputcsv($fp, $fields);
}

fclose($fp);


//
header("Content-type: application/octet-stream");
header("Content-Disposition: attachment; filename=\"".$file_name."\"");

echo file_get_contents($csv_file); 
?>