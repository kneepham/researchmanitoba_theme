<?php

//
include '../../../../wp-config.php';

/*function ttruncat($text,$numb) {
	if (strlen($text) > $numb) {
	  $text = substr($text, 0, $numb);
	  $text = substr($text,0,strrpos($text," "));
	  $etc = " ...";
	  $text = $text.$etc;
	  }
	return $text;
}*/

$cxn = mysqli_connect(DB_HOST, DB_USER, DB_PASSWORD, DB_NAME) or die(mysqli_error($cxn));

$request_stuff = explode('|', $_REQUEST['id']);

foreach ($request_stuff as $key => $value) {
	$request_stuff[$key] = trim($value);
	$request_stuff[$key] = strip_tags($request_stuff[$key]);
	$request_stuff[$key] = addslashes($request_stuff[$key]);
}

//
$query = "SELECT * FROM mhrc_recipients WHERE r_category='".$request_stuff[0]."' && r_year=".$request_stuff[1]." && trashed='n' ORDER BY r_category";
$result = mysqli_query($cxn, $query) or die($query.': '.mysqli_error($cxn));

while ($data = mysqli_fetch_object($result)) :
	$job_title = $data->department;
	
	if ($data->faculty != '') {
		if ($job_title != '') {
			$job_title .= ', ';
		}
		
		$job_title .= $data->faculty;
	}
	
	if ($data->school != '') {
		if ($job_title != '') {
			$job_title .= ', ';
		}
		
		$job_title .= $data->school;
	}
?>
	
    <li>
    	<div class="award-title">Award: <?php echo $data->award_title; ?></div>
		<h2><?php echo $data->r_name; ?></h2>
		<h3><?php echo $job_title; ?></h3>
		<p><strong><em><?php echo $data->project_title; ?>:</em></strong> <?php echo ttruncat($data->writeup, 160); ?> <a href="profile/?id=<?php echo $data->id; ?>" class="more">More -></a></p>
	</li>

<?php endwhile; ?>