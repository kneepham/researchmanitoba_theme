<?php
/**
 * Template Name: Home
 */
?>

<?php get_header('front'); ?>

<div class="container-main" >
<?php while ( have_posts() ): the_post(); ?>
    <?php get_template_part( 'template-parts/components/slider' ); ?>
<?php endwhile ?>
</div>

<div class="container-main container-main__flex" >

    <div class="container-main__left">
        <?php get_template_part( 'template-parts/components/sidebar' ); ?>
    </div>

    <div class="container-main__right">
        <?php while ( have_posts() ): the_post(); ?>
        	<div class="content">
        		<?php the_content() ?>
        	</div>
            <?php get_template_part( 'template-parts/components/section-loop' ); ?>
        <?php endwhile ?>
    </div>

</div>

<?php get_footer('front'); ?>
