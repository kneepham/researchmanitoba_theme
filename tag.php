<?php get_header(); ?>

<div class="main_content">
<img src="<?php bloginfo('template_url'); ?>/template/img/blog.jpg" class="sub_banner">

	<div class="holder <?php echo $template_colour; ?>">
		<div class="copy_content">

				<h2 class="blogTop"><?php
					printf( __( 'Tag Archives: %s', 'twentyten' ), '' . single_tag_title( '', false ) . '' );
				?></h2>

<?php
 get_template_part( 'loop', 'tag' );
?>

		</div>

		<aside>
		<?php get_sidebar(); ?>
		</aside>
	</div>
</div>

<div class="clear_both"></div>
                
<?php get_footer(); ?>