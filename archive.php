<?php get_header(); ?>

<div class="main_content">
<img src="<?php bloginfo('template_url'); ?>/template/img/blog.jpg" class="sub_banner">

	<div class="holder <?php echo $template_colour; ?>">
		<div class="copy_content">

<?php if ( have_posts() )
		the_post();
?>

			<h2 class="blogTop">
<?php if ( is_day() ) : ?>
				<?php printf( __( 'Daily Archives: %s', 'twentyten' ), get_the_date() ); ?>
<?php elseif ( is_month() ) : ?>
				<?php printf( __( 'Monthly Archives: %s', 'twentyten' ), get_the_date('F Y') ); ?>
<?php elseif ( is_year() ) : ?>
				<?php printf( __( 'Yearly Archives: %s', 'twentyten' ), get_the_date('Y') ); ?>
<?php else : ?>
				<?php _e( 'Blog Archives', 'twentyten' ); ?>
<?php endif; ?>
			</h2>

<?php 
	rewind_posts();
	get_template_part( 'loop', 'archive' );
?>


		</div>

		<aside>
		<?php get_sidebar(); ?>
		</aside>
	</div>
</div>

<div class="clear_both"></div>
                
<?php get_footer(); ?>