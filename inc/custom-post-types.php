<?php
/********************************
* Slider
*********************************/
function create_cpt_slider() {
	$labels = array(
		'name' => _x('Slider', 'post type general name'),
		'singular_name' => _x('Slider', 'post type singular name'),
		'add_new' => _x('Add New', 'Slider'),
		'add_new_item' => __('Add New Slider'),
		'edit_item' => __('Edit Slider'),
		'new_item' => __('New Slider'),
		'all_items' => __('All Slider'),
		'view_item' => __('View Slider'),
		'search_items' => __('Search Slider'),
		'not_found' =>  __('No Slider found'),
		'not_found_in_trash' => __('No Slider found in Trash'), 
		'parent_item_colon' => '',
		'menu_name' => 'Sliders'
	);

	$args = array(
		'labels' => $labels,
		'exclude_from_search' => true,
		'public' => false,
		'publicly_queryable' => false,
		'show_ui' => true, 
		'show_in_menu' => true, 
		'query_var' => false,
		'has_archive' => false, 
		'hierarchical' => false,
		'supports' => array('title'),
		'capability_type' => 'post',
		'menu_icon' => 'dashicons-arrow-right'
	); 

	register_post_type('honest_slider',$args);
}

add_action( 'init', 'create_cpt_slider' );

/********************************
* Wave Magazine
*********************************/
function create_cpt_magazine() {

	$labels = array(
		'name' => _x('Magazine', 'post type general name'),
		'singular_name' => _x('Magazine', 'post type singular name'),
		'add_new' => _x('Add New', 'Magazine'),
		'add_new_item' => __('Add New Magazine'),
		'edit_item' => __('Edit Magazine'),
		'new_item' => __('New Magazine'),
		'all_items' => __('All Magazine'),
		'view_item' => __('View Magazine'),
		'search_items' => __('Search Magazine'),
		'not_found' =>  __('No Magazine found'),
		'not_found_in_trash' => __('No Magazine found in Trash'), 
		'parent_item_colon' => '',
		'menu_name' => 'Magazines & Reports'
	);

	$args = array(
		'labels' => $labels,
		'exclude_from_search' => true,
		'public' => false,
		'publicly_queryable' => false,
		'show_ui' => true, 
		'show_in_menu' => true, 
		'query_var' => false,
		'has_archive' => false, 
		'hierarchical' => false,
		'supports' => array('title'),
		'capability_type' => 'post',
		'menu_icon' => 'dashicons-analytics'
	); 

	register_post_type('magazine',$args);

}
add_action( 'init', 'create_cpt_magazine' );


/********************************
* Wave Magazine Taxonomy/Catgory
*********************************/
function register_magazine_taxonomy() {
	$labels = array(
		'name' => _x( 'Category', 'taxonomy general name' ),
		'singular_name' => _x( 'Category', 'taxonomy singular name' ),
		'search_items' =>  __( 'Search Categories' ),
		'all_items' => __( 'All Categories' ),
		'parent_item' => __( 'Parent Categories' ),
		'parent_item_colon' => __( 'Parent Categories:' ),
		'edit_item' => __( 'Edit Category' ), 
		'update_item' => __( 'Update Category' ),
		'add_new_item' => __( 'Add New Category' ),
		'new_item_name' => __( 'New Category Name' ),
		'menu_name' => __( 'Categories' ),
	); 	

	register_taxonomy('magazine_category',array('magazine'), array(
		'hierarchical' => true,
		'labels' => $labels,
		'show_ui' => true,
		'query_var' => true,
		'rewrite' => false,
	));
}

add_action( 'init', 'register_magazine_taxonomy' );


add_filter( 'manage_magazine_posts_columns', 'set_custom_edit_magzine_columns' );
function set_custom_edit_magzine_columns($columns) {
	unset($columns['date']);
    $columns['cat'] = __( 'Published Date', 'your_text_domain' );

    return $columns;
}

add_action( 'manage_posts_custom_column' , 'custom_columns', 10, 2 );

function custom_columns( $column, $post_id ) {
	global $post;

	switch ( $column ) {
		case 'cat':

			$date = get_field('upload_date',$post->ID);

			echo $date;

			
			break;
	}
}


add_filter( 'manage_edit-magazine_sortable_columns', 'edit_magazine_sortable_columns' );

function edit_magazine_sortable_columns( $columns ) {

	$columns['cat'] = 'cat';

	return $columns;
}


/**
 * Display a custom taxonomy dropdown in admin
 * @author Mike Hemberger
 * @link http://thestizmedia.com/custom-post-type-filter-admin-custom-taxonomy/
 */
add_action('restrict_manage_posts', 'tsm_filter_post_type_by_taxonomy');
function tsm_filter_post_type_by_taxonomy() {
	global $typenow;
	$post_type = 'magazine'; // change to your post type
	$taxonomy  = 'magazine_category'; // change to your taxonomy
	if ($typenow == $post_type) {
		$selected      = isset($_GET[$taxonomy]) ? $_GET[$taxonomy] : '';
		$info_taxonomy = get_taxonomy($taxonomy);
		wp_dropdown_categories(array(
			'show_option_all' => __("Show All {$info_taxonomy->label}"),
			'taxonomy'        => $taxonomy,
			'name'            => $taxonomy,
			'orderby'         => 'name',
			'selected'        => $selected,
			'show_count'      => true,
			'hide_empty'      => true,
		));
	};
}
/**
 * Filter posts by taxonomy in admin
 * @author  Mike Hemberger
 * @link http://thestizmedia.com/custom-post-type-filter-admin-custom-taxonomy/
 */
add_filter('parse_query', 'tsm_convert_id_to_term_in_query');
function tsm_convert_id_to_term_in_query($query) {
	global $pagenow;
	$post_type = 'magazine'; // change to your post type
	$taxonomy  = 'magazine_category'; // change to your taxonomy
	$q_vars    = &$query->query_vars;
	if ( $pagenow == 'edit.php' && isset($q_vars['post_type']) && $q_vars['post_type'] == $post_type && isset($q_vars[$taxonomy]) && is_numeric($q_vars[$taxonomy]) && $q_vars[$taxonomy] != 0 ) {
		$term = get_term_by('id', $q_vars[$taxonomy], $taxonomy);
		$q_vars[$taxonomy] = $term->slug;
	}
}