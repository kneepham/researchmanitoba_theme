<?php

/**
 *	Creates a img tag and set the apporiate attributes. 
 *	@param (REQUIRED)Array - ACF Image Array, (REQUIRED)String , Array - match the following keys - class, attributes, extra_attributes
 */
function get_image_acf($acf_image, $size = null, $options = null)
{

	$img = "<img";
	$attributes = array('alt','title','description','caption');

	if (isset($options['attributes']) && $options['attributes'] && is_array($options['attributes']))
	{
		$attributes = $options['attributes'];
	}

	if (isset($options['class']) && $options['class'] && is_string($options['class']))
	{
		$class = $options['class'];
		$img .= " class='" . $class . "'";
	}

	if (isset($acf_image['sizes']) && $size && isset($acf_image['sizes'][$size]) && ($src = $acf_image['sizes'][$size]))
	{
		$img .= " src='" . $src . "'";
	}
	else if($src = $acf_image['url'])
	{
		$img .= " src='" . $src . "'";
	}

	foreach($attributes as $attribute)
	{
		if (isset($acf_image[$attribute]) && ($attr = $acf_image[$attribute]))
		{
			$img .= " " . $attribute . "='" . $attr . "'";
		}
	}

	if (isset($options['extra_attributes']) && $options['extra_attributes'] && is_array($options['extra_attributes']))
	{
		$extra_attributes = $options['extra_attributes'];

		foreach($extra_attributes as $key => $extra_attribute)
		{
			if (is_string($extra_attribute) && $extra_attribute)
			{
				$img .= " " . $key . "='" . $extra_attribute . "'";
			}
		}
	}

	$img .= " >";

	return $img;
}

function get_excluded_magazine_post($post_id){
	
	static $magainze_posts;

	if (!isset($magainze_posts))
		$magainze_posts = array();

	if (is_numeric($post_id))
	{
		$magainze_posts[] = $post_id;
	}
	else
	{
		return $magainze_posts;
	}

}