<?php

// ACF Options pages
if( function_exists('acf_add_options_page') ) {
	
	acf_add_options_page(array(
		'page_title' 	=> 'Email Template',
		'menu_title'	=> 'Email Template',
		'menu_slug' 	=> 'email-template',
		'capability'	=> 'edit_posts',
	));

	acf_add_options_page(array(
		'page_title' 	=> 'Options',
		'menu_title'	=> 'Options',
		'menu_slug' 	=> 'options',
		'capability'	=> 'edit_posts',
	));
	
}

add_filter('acf/settings/remove_wp_meta_box', '__return_false');