<?php
/**
 * Template Name: Programs Innovation
 */
get_header(); 
 
if ( have_posts() ) {
	while ( have_posts() ) {
		the_post();
	}
}

$the_parent = SUB_FUNDING_PROGRAMS;
$id = get_the_ID();

$title = get_the_title();
$content = get_the_content();

$ancestors = get_post_ancestors( $id );
$ancestor_id = end($ancestors);

$template_colour = get_post_meta($ancestor_id, 'template_colour', true);
if (!$template_colour) {
	$template_colour = 'purple';
}

$banner = get_post_meta($id, 'banner', true);
if (!$banner) {
    $banner = get_post_meta($ancestor_id, 'banner', true);
}
if( substr( $banner, 0, 4 ) === "http" ) {
    $banner_out = $banner;
} else {
    $banner_out = get_bloginfo('url') . $banner;
}

$sidebar = get_post_meta($the_parent, 'sidebar', true);
$sidebar_am = $wpdb->get_results("SELECT * FROM $wpdb->postmeta WHERE meta_key='sidebar' AND post_id=".$id." ORDER BY meta_id ASC");

$pages = get_posts('numberposts=500&orderby=menu_order&order=ASC&post_type=page&post_parent='.$the_parent);


?>

				<div class="main_content">
               
                    
					<div class="holder blue">
                    	<div class="copy_content">
                        	<h1>Programs</h1>                                                      
                            
                            <div class="program_profile">
                                <h2><?php echo $title; ?></h2>
                                
                                <?php echo $content; ?>
                            </div>
                        </div>
                        
                        <aside>
                            <ul>
                                <li>
                                    <a href="https://researchmanitoba.ca/funding-opportunities/innovation-proof-of-concept-grant/" class="" target="_blank">
                                        Innovation Proof of Concept Grant                                    
                                    </a>
                                </li>
                            </ul>
 
							<?php 
                            $defaults = array(
                                'theme_location'  => 'health-research',
                            );
                             
                            //wp_nav_menu( $defaults );
                            ?>

                        </aside>
                    </div>
                </div>
                
                <div class="clear_both"></div>
                
<?php get_footer(); ?>
