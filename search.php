<?php
/**
 * The template for displaying Search Results pages.
 *
 * @package WordPress
 * @subpackage blankSlate
 * @since blankSlate 3.1
 */

get_header(); 
?>

	
    <div class="main_content blog">
        <img src="<?php bloginfo('template_url'); ?>/template/img/blog.jpg" class="sub_banner">
    
        <div class="holder teal_b">
            <div class="copy_content">
                <h2 class="blogTop"><?php printf( __( 'Search Results for: %s', 'twentyten' ), '' . get_search_query() . '' ); ?></h2>
                
                <?php if ( have_posts() ) : ?>
            
                    <?php
                    /* Run the loop for the search to output the results.
                     * If you want to overload this in a child theme then include a file
                     * called loop-search.php and that will be used instead.
                     */
                     get_template_part( 'loop', 'search' );
                    ?>
            
                <?php else : ?>
            
                    <div id="post-0" class="post no-results not-found">
                        <h2><?php _e( 'Nothing Found', 'twentyten' ); ?></h2>
                        
                        <p><?php _e( 'Sorry, but nothing matched your search criteria. Please try again with some different keywords.', 'twentyten' ); ?></p>
                            <?php get_search_form(); ?>
                        
                    </div><!-- #post-0 -->
            
                <?php endif; ?>
            </div>
    
            <aside>
            
            </aside>
        </div>
	</div>

	<div class="clear_both"></div>


<?php 
get_footer(); 
?>